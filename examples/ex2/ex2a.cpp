#include <iostream>
#include <World.hpp>
#include <SimulationVectorial.hpp>
#include <AlgoAddIntervalled.hpp>
#include <AlgoPosRandomUniform.hpp>
#include <AlgoGrowConstant.hpp>
#include <RenderCsvProgress.hpp>
#include <RenderCsvClusters.hpp>
#include <RenderCsvFit.hpp>
#include <FitterPolynomial.hpp>
#include <RenderPngProgress.hpp>
#include <RenderCtiogaProgress.hpp>
#include <ClusterColorRandomAcid.hpp>

using namespace std;
using namespace clustersim;


/**
 * This example perform the simulation of the nucleation proces of a 2D film with
 * size of 10cm x 10cm.
 * 
 * The nucleation process is a constant one (are created 2 clusters every second).
 * 
 * All units are in SI format (if not speciefed differently)
 * 
 * 
 */
int main (int argc, char *argv[])
{
	// Creating the world
	World w = World({0.1, 0.1}); // 2D world, 10cm x 10cm
	
	SimulationVectorial s = SimulationVectorial(w);
	
	
	s.setAlgoAdd(new AlgoAddIntervalled(2, 1)); // Adding 2 clusters every second
	s.setAlgoPos(new AlgoPosRandomUniform());
	s.setAlgoGrow(new AlgoGrowConstant(0.001)); // Grow by 1 mm every second
	
	s.addFitter(new FitterPolynomial());
	
	s.addRender(new RenderCsvProgress("grow"));
	s.addRender(new RenderCsvClusters ("clusters"));
	s.addRender(new RenderCsvFit ("fit"));

	s.addRender(new RenderPngProgress("render", new ClusterColorRandomAcid()));
	s.addRender(new RenderCtiogaProgress("plot"));

	s.setOutputDir("ex2a");
	
	s.start();

    double target= 0;
    while (true)
    {
        s.execute(target += 0.05);
        cout << "Time : " << s.getCurrentTime()  <<" s, ratio: " << s.getTransformedRatio() * 100 << " %" <<  endl;

        if (target >= 1.) break;
    }

	s.stop();
}
