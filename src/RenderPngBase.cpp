#include <stdexcept>
#include <vector>

#include "SimulationPixeled.hpp"
#include "ClusterColorBase.hpp"
#include "RenderPngBase.hpp"
#include "World.hpp"

using namespace std;
using namespace clustersim;

RenderPngBase::RenderPngBase(bool periodic, string basename) : RenderPngBase(periodic, basename, nullptr)
{
}

RenderPngBase::RenderPngBase(bool periodic, string basename, ClusterColorBase* clusterColor) : RenderBase(periodic), basename(basename), clusterColor(clusterColor)
{
}

void RenderPngBase::initialize(SimulationBase &simulation)
{
    RenderBase::initialize(simulation);

    checkConfig(simulation);

    World& world = simulation.getWorld();

    // Adding missing cuts
    for (unsigned int d = 0; d < world.dimensions; d++)
    {
        if ((d != axisI) && (d != axisJ))
        {
            if (cuts.find(d) == cuts.end())
                cuts[d] = world.sizes[d] / 2.;
        }
    }


    if (clusterColor == nullptr)
    {
        clusterColor = &defaultClusterColor;
    }

    clusterColor->initialize(simulation);

    if (preferedWidth == 0. and preferedHeight == 0.)
    {
        width  = simulation.getPreferredPixelSize(axisI);
        height = simulation.getPreferredPixelSize(axisJ);;
    }
    else if (preferedWidth > 0.)
    {
        width  = preferedWidth;
        height = width * world.sizes[axisJ] / world.sizes[axisI];
    }
    else
    {
        height = preferedHeight;
        width  = height * world.sizes[axisI] / world.sizes[axisJ];
    }
}

void RenderPngBase::finalize(SimulationBase &simulation)
{
    if (clusterColor != nullptr)
    {
        clusterColor->finalize(simulation);
    }

    RenderBase::finalize(simulation);
}

bool RenderPngBase::hasCutLevel(unsigned char axis)
{
    return cuts.find(axis) == cuts.end();
}

float RenderPngBase::getCutLevel(unsigned char axis)
{
    if (cuts.find(axis) == cuts.end())
        return 0;
    else
        return cuts[axis];
}

void RenderPngBase::setCutLevel(unsigned char axis, float level)
{
    cuts[axis] = level;
}


void RenderPngBase::checkConfig(SimulationBase &simulation)
{
    World &world = simulation.getWorld();

	if (world.dimensions < 2)
		 throw invalid_argument("A PNG Render can not be used with 1D worlds");

    if (axisI >= world.dimensions)
        throw invalid_argument("The value of 'axisI' bust be between " + to_string(0) + " and " + to_string(world.dimensions - 1));

    if (axisJ >= world.dimensions)
        throw invalid_argument("The value of 'axisJ' bust be between " + to_string(0) + " and " + to_string(world.dimensions - 1));

    if (axisI == axisJ)
        throw invalid_argument("The value of 'axisI' must be different of the value of 'axisJ'");

    for (pair<unsigned char,float> cut: cuts)
    {
        if (cut.second < 0)
            throw invalid_argument("In cutLevels is not allowed a negative value.");
        if (cut.first >= world.dimensions)
            throw invalid_argument("In cutLevels there is an axis with id " + to_string(cut.first) + " but only id between 0 and " + to_string(world.dimensions - 1) + " are allowed.");
    }

    for (unsigned int d = 0; d < world.dimensions; d++)
    {
        if ((d != axisI) && (d != axisJ))
        {

            // Missing cuts will be automatically added
            //if (cuts.find(d) == cuts.end())
            //    throw invalid_argument("It is impossible to make a 2D render of a " + to_string(world.dimensions) + "D volume: use 'setCutLevel' to set the cutting plane of the volume");
            //else
            if (cuts[d] < 0 || cuts[d] > world.sizes[d])
                throw invalid_argument("The value of 'setCutLevel' for axis " + to_string(d) + " must be between " + to_string(0.f) + " and " + to_string(world.sizes[d]) + ": current value is " + to_string(cuts[d]));
            else if (d == axisI)
                throw invalid_argument("The axis " + to_string(d) + " was specified in both 'setCutLevel' and in 'setAxisI'");
            else if (d == axisJ)
                throw invalid_argument("The axis " + to_string(d) + " was specified in both 'setCutLevel' and in 'setAxisJ'");
        }

    }


    bool hasPreferedWidth  = preferedWidth  != 0;
    bool hasPreferedHeight = preferedHeight != 0;

    if (hasPreferedWidth && hasPreferedHeight)
        throw invalid_argument("Specify just one of 'preferedWidth' or 'preferedHeight'. You can not specify both.");

}

