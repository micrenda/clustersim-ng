#include <ext/alloc_traits.h>

#include "AlgoPosRandomUniform.hpp"
#include "Cluster.hpp"
#include "SimulationBase.hpp"
#include "World.hpp"

using namespace clustersim;

void AlgoPosRandomUniform::initialize(SimulationBase& simulation)
{
	AlgoPosRandom::initialize(simulation);
	
	for (unsigned char d = 0; d < simulation.getWorld().dimensions; d++)
	{
		distributions.push_back(uniform_real_distribution<float>(0, simulation.getWorld().sizes[d]));
	}
	
}


void AlgoPosRandomUniform::place(SimulationBase& simulation, Cluster& cluster)
{

	for (unsigned char d = 0; d < simulation.getWorld().dimensions; d++)
	{
		cluster.center[d] = distributions[d](generator);
	}
}
