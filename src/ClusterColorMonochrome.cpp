#include "ClusterColorMonochrome.hpp"

using namespace clustersim;

ClusterColorMonochrome::ClusterColorMonochrome()
{
}

ClusterColorMonochrome::ClusterColorMonochrome(unsigned int color): color(color)
{

}



ClusterColorMonochrome::~ClusterColorMonochrome()
{

}

void ClusterColorMonochrome::initialize(clustersim::SimulationBase &simulation)
{
    ClusterColorBase::initialize(simulation);
}

void ClusterColorMonochrome::finalize(clustersim::SimulationBase &simulation)
{
    ClusterColorBase::finalize(simulation);
}
unsigned int ClusterColorMonochrome::getColor(clustersim::SimulationBase &simulation, clustersim::Cluster &cluster)
{
    return color;
}