#include <iostream>
#include <stdexcept>
#include <vector>

#include "FitterBase.hpp"
#include "RenderCsvFit.hpp"
#include "SimulationBase.hpp"


using namespace std;
using namespace clustersim;

RenderCsvFit::RenderCsvFit(string basename): RenderCsvBase(false, basename)
{
}

void RenderCsvFit::initialize(SimulationBase& simulation)
{
	RenderCsvBase::initialize(simulation);
	
	if(!simulation.getFitters().size())
		throw logic_error( "A 'RenderCsvFit' was used, but not fitter was added to the current simulation. Please use simulation->addFitter() to set a fitter for the simulation" );
}

void RenderCsvFit::execute(SimulationBase& simulation)
{
	RenderCsvBase::execute(simulation);
	// Nothing to do
}

void RenderCsvFit::finalize(SimulationBase& simulation)
{
	vector<FitterBase*> fitters = simulation.getFitters();
	if (fitters.size())
	{
		csvfile.seekp (0, ios_base::end);
		if (csvfile.tellp() == 0)
			csvfile << "points,fit_n,fit_k,fit_mse,fit_rmse,t_min,t_max,transformed_ratio_min,transformed_ratio_max" << endl;
		
		for (FitterBase* fitter: fitters)
			csvfile << 
				fitter->getPointsCount() << "," <<
				fitter->getFitN() << "," <<
				fitter->getFitK() << "," << 
				fitter->getFitMSE() << "," <<
				fitter->getFitRMSE() << "," <<
				fitter->getTimeMin() << "," <<
				fitter->getTimeMax() << "," <<
				fitter->getVolumeRangeFrom() << "," <<
				fitter->getVolumeRangeTo() << endl;
	}
		
	RenderCsvBase::finalize(simulation);
}

