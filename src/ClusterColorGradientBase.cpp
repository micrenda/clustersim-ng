#include <cmath>
#include <stdexcept>
#include <vector>

#include "ClusterColorGradientBase.hpp"

using namespace clustersim;

ClusterColorGradientBase::ClusterColorGradientBase(std::vector<unsigned int>& colors): colors(colors)
{
	if (colors.size() < 2)
		throw std::invalid_argument ("At least two colors are needed to create a gradient");
}

ClusterColorGradientBase::~ClusterColorGradientBase()
{
	
}

unsigned int ClusterColorGradientBase::getColorGradRgb(float value)
{
	
	float i = value    - valueStart;
	float n = valueEnd - valueStart;
	
	unsigned int ranges = colors.size() - 1;
	
	unsigned int start_rgb;
	unsigned int end_rgb;
	
	float i_r;
	float n_r;
	
	
	if (i <= 0)
	{
		start_rgb = colors[0];
		end_rgb   = colors[1];
		
		n_r = 1. * n / ranges;
		i_r = 0.;
	}
	else if (i >= n)
	{
		start_rgb   = colors[colors.size() - 2];
		end_rgb   	= colors[colors.size() - 1];
		
		n_r = 1. * n / ranges;
		i_r = 1. * n / ranges - 1;
	}
	else
	{
		unsigned int lower_index = floor(1. * ranges * i / n);  
		start_rgb = colors[lower_index];
		end_rgb	  = colors[lower_index+1];
		n_r = 1. * n / ranges;
		i_r = fmod(i, n_r);
	}

	 
	
    unsigned short start_red   = (start_rgb & 0xff0000) >> 16;
    unsigned short start_green = (start_rgb & 0x00ff00) >>  8;
    unsigned short start_blue  = (start_rgb & 0x0000ff) >>  0;

    unsigned short end_red     = (end_rgb & 0xff0000) >> 16;
    unsigned short end_green   = (end_rgb & 0x00ff00) >>  8;
    unsigned short end_blue    = (end_rgb & 0x0000ff) >>  0;

    unsigned short color_red   = start_red   + round(1.f * (end_red   - start_red  ) * i_r / n_r);
    unsigned short color_green = start_green + round(1.f * (end_green - start_green) * i_r / n_r);
    unsigned short color_blue =  start_blue  + round(1.f * (end_blue  - start_blue ) * i_r / n_r);

    return (color_red << 16) + (color_green << 8) + (color_blue << 0);
}
