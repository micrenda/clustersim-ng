#include <algorithm>

#include "AlgoGrowConstant.hpp"
#include "ClockTimer.hpp"
#include "SimulationBase.hpp"
#include "Cluster.hpp"

using namespace clustersim;

AlgoGrowConstant::AlgoGrowConstant(float speed):  speed(speed)
{
	
}

float AlgoGrowConstant::grow(SimulationBase& simulation, Cluster& cluster)
{
	return std::max(simulation.getCurrentTime() - cluster.lastGrowTime, 0.f) * speed;
}



float AlgoGrowConstant::getNextExecution(SimulationBase& simulation)
{
	return simulation.getCurrentTime() + simulation.defaultClock.getPeriod();
}
