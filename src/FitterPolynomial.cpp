#include <gsl/gsl_fit.h>
#include <map>
#include <cmath>
#include <utility>

#include "FitterPolynomial.hpp"

namespace clustersim
{
    class SimulationBase;
}  // namespace clustersim

using namespace std;
using namespace clustersim;

FitterPolynomial::FitterPolynomial() : FitterBase(true)
{
}

FitterPolynomial::FitterPolynomial(float volumeRangeFrom, float volumeRangeTo) : FitterBase(volumeRangeFrom, volumeRangeTo, true)
{

}


void FitterPolynomial::finalize(SimulationBase &simulation)
{

    unsigned int n = logValues.size();

    double x[n];
    double y[n];

    unsigned int i = 0;

    for (pair<float, float> p: logValues)
    {
        x[i] = (double) p.first;
        y[i] = (double) p.second;
        i++;
    }

    double c0, c1;

    gsl_fit_linear(x, 1, y, 1, n, &c0, &c1, &fitCov00, &fitCov01, &fitCov11, &fitSumQ);

    fitK = exp((float) c0);
    fitN = (float) c1;

    fitMSE = 1.0 / n * fitSumQ;
    fitRMSE = sqrt(fitMSE);

    FitterBase::finalize(simulation);
}

