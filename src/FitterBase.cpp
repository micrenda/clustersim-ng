#include <utility>

#include "ClockTimer.hpp"
#include "FitterBase.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

FitterBase::FitterBase(bool periodic): HasVolumeRange(0.15, 0.85), periodic(periodic)
{

}


FitterBase::FitterBase(float volumeRangeFrom, float volumeRangeTo, bool periodic) : HasVolumeRange(volumeRangeFrom, volumeRangeTo), periodic(periodic)
{

}



FitterBase::~FitterBase()
{

}

void FitterBase::initialize(SimulationBase& simulation)
{
	logValues.clear();
}

void FitterBase::execute(SimulationBase& simulation)
{

		logValues.insert(pair<float,float>(simulation.getLogCurrentTime(), simulation.getLogTransformedRatio()));
	
		float currentTime = simulation.getCurrentTime();
		if (currentTime < timeMin) timeMin = currentTime;
		if (currentTime > timeMax) timeMax = currentTime;

}

void FitterBase::finalize(SimulationBase& simulation)
{
	// Does nothing. It should be overrided by subclasses to do something useful
}


float FitterBase::getNextExecution(SimulationBase& simulation)
{
	if (periodic)
		return simulation.getCurrentTime() + simulation.defaultClock.getPeriod();
	else
		return -1;
}

unsigned int FitterBase::getPointsCount()
{
	return logValues.size();
}
