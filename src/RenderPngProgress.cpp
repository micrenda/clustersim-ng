#include <png++/image.hpp>
#include <map>
#include <png++/rgb_pixel.hpp>          // for rgb_pixel
#include <vector>                       // for vector

#include "RenderPngProgress.hpp"
#include "SimulationBase.hpp"           // for SimulationBase
#include "ClusterColorBase.hpp"
#include "World.hpp"


using namespace std;
using namespace clustersim;
using namespace png;

namespace clustersim { class Cluster; }

void RenderPngProgress::initialize(SimulationBase& simulation)
{
	RenderPngBase::initialize(simulation);
	
	currentFrame = 0;
}


void RenderPngProgress::execute(SimulationBase& simulation)
{
	RenderPngBase::execute(simulation);

	World world = simulation.getWorld();
	
	image<rgb_pixel> png(width, height);
	
	vector<float> point;
	
	
	for (unsigned int d = 0; d < world.dimensions; d++)
	{
		if (cuts.find(d) == cuts.end())
			point.push_back(0.);
		else
			point.push_back(cuts[d]);
	}
	
	for (unsigned int i = 0; i < width; i++)
	{
		point[axisI] = 1.f * i / width * world.sizes[axisI];
		
		for (unsigned int j = 0; j < height; j++)
		{
			point[axisJ] = 1.f * j / height * world.sizes[axisJ];
			Cluster* cluster = simulation.getClusterAtPoint( point );

            if (cluster != nullptr)
            {
                unsigned int color = clusterColor->getColor(simulation, *cluster);
                png.set_pixel(i, j, rgb_pixel((color & 0xff0000) >> 16, (color & 0x00ff00) >> 8, (color & 0x0000ff) >> 0));
            }
            else
                png.set_pixel(i, j, rgb_pixel((backgroundColor & 0xff0000) >> 16, (backgroundColor & 0x00ff00) >> 8, (backgroundColor & 0x0000ff) >> 0));
			
		}
	}
	
	png.write(buildFilename(simulation, currentFrame));
	currentFrame++;
}


void RenderPngProgress::finalize(SimulationBase& simulation)
{
	RenderPngBase::finalize(simulation);
}


string RenderPngProgress::buildFilename(const SimulationBase &simulation, unsigned int i)
{
    string filename = basename + "_" + to_string(i) + ".png";

    if (!simulation.getOutputDir().empty())
        filename = simulation.getOutputDir() + "/" + filename;

    return filename;
}
