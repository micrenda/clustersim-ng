#include <iostream>
#include <vector>

#include "Cluster.hpp"
#include "RenderCsvClusters.hpp"
#include "SimulationBase.hpp"
#include "World.hpp"

using namespace std;
using namespace clustersim;


void RenderCsvClusters::finalize(SimulationBase& simulation)
{
	World& world = simulation.getWorld();
	
	csvfile << "#id, creation_time, radius, volume, volume_ratio";
	
	for (unsigned short d = 0; d < world.dimensions; d++)
		csvfile << ", " << "center_x" << d+1;
	csvfile << endl;
  
	
	for (unsigned int i = 0; i < simulation.getClusterCount(); i++)
	{
		Cluster& cluster = simulation.getCluster(i);
		
		csvfile
			<< i 							  << ", "
			<< cluster.creationTime           << ", "
			<< cluster.radius           	  << ", "
			<< simulation.getClusterVolume(cluster) << ", "
			<< simulation.getClusterVolume(cluster) / simulation.getTotalVolume();
			
		for (unsigned short d = 0; d < world.dimensions; d++)
			csvfile << ", " << cluster.center[d];
		
		csvfile << endl;
	}
  
  
	RenderCsvBase::finalize(simulation);
}
