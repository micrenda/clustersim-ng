#include <pstreams/pstream.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdexcept>

#include "ClockTimer.hpp"
#include "RenderBase.hpp"
#include "SimulationBase.hpp"


using namespace clustersim;
using namespace std;

RenderBase::RenderBase(bool periodic): HasVolumeRange(), periodic(periodic)
{

}

RenderBase::~RenderBase()
{
	// Nothing to do. Don't forget to implement when needed in subclasses
}

void RenderBase::initialize(SimulationBase& simulation)
{
	if (periodic)
	{
		if (period == 0)
			period = simulation.defaultClock.getPeriod();
	}

    simulation.makeOutputDir();
}

void RenderBase::execute(SimulationBase& simulation)
{

}

void RenderBase::finalize(SimulationBase& simulation)
{

}


float RenderBase::getNextExecution(SimulationBase& simulation)
{
	if (periodic)
		return simulation.getCurrentTime() + period;
	else
		return -1;
}


void RenderBase::executeProcess(string cmd)
{

	redi::ipstream cmdstream(cmd, redi::pstreams::pstdout | redi::pstreams::pstderr);

    string errmsgComplete = "";
	string errmsg;
	
	while (getline(cmdstream.err(), errmsg))
	{
        errmsgComplete += errmsg + "\n";

		if (cmdstream.eof())
                cmdstream.clear();
    }   
    
    cmdstream.close();
    
    int retval = cmdstream.rdbuf()->status();
    if ( retval != 0)
		 throw std::runtime_error("sub command exited with value " + to_string(retval) + ": " + cmd + "\n\n" + errmsgComplete );
        

}


