#include "AlgoAddIntervalled.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

AlgoAddIntervalled::AlgoAddIntervalled(unsigned int quantity, float interval): quantity(quantity), interval(interval)
{	
	
}

int AlgoAddIntervalled::add(SimulationBase& simulation)
{
	return quantity;
}

float AlgoAddIntervalled::getNextExecution(SimulationBase& simulation)
{
	return simulation.getCurrentTime() + interval;
}
