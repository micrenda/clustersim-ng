#include "AlgoBase.hpp"

namespace clustersim {
class SimulationBase;
}  // namespace clustersim


using namespace clustersim;

AlgoBase::AlgoBase()
{

}
AlgoBase::~AlgoBase()
{
	// Nothing to do. Don't forget to implement when needed in subclasses
}

void AlgoBase::initialize(SimulationBase& simulation)
{

}

void AlgoBase::finalize(SimulationBase& simulation)
{

}
