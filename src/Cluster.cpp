#include "Cluster.hpp"

using namespace clustersim;

Cluster::Cluster(unsigned char dimensions, unsigned int id, float creationTime): 
	id(id),
	radius(0),
	growing(true),
	creationTime(creationTime),
	lastGrowTime(creationTime)
{
	this->center.assign (dimensions, 0);
}
