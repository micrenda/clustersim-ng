#include <cmath>
#include <stdio.h>
#include <string>

#include "FitterBase.hpp"
#include "RenderCtiogaProgress.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

RenderCtiogaProgress::RenderCtiogaProgress(string basename): RenderCsvProgress(basename)
{

}

void clustersim::RenderCtiogaProgress::finalize(SimulationBase &simulation)
{
    RenderCsvProgress::finalize(simulation);

    createCtLin(simulation);
    createCtLog(simulation);

    string filenameCsv    = buildFilename(simulation);
    string filenameCt2Lin = buildCt2Filename(simulation, "lin");
    string filenameCt2Log = buildCt2Filename(simulation, "log");

    string outputDir = simulation.getOutputDir();

    executeProcess("ctioga2 --csv --load '" + filenameCsv + "' -f '" + filenameCt2Lin + "' " + (!outputDir.empty() ? "--output-directory " + outputDir : "") + " --name '" + basename + "-lin'");
    executeProcess("ctioga2 --csv --load '" + filenameCsv + "' -f '" + filenameCt2Log + "' " + (!outputDir.empty() ? "--output-directory " + outputDir : "") + " --name '" + basename + "-log'");

}

void RenderCtiogaProgress::createCtLin(SimulationBase& simulation)
{
    ofstream ct;
    ct.open(buildCt2Filename(simulation, "lin"));
    ct << "title 'Cluster grow'" 													<< endl;
    ct << "legend-inside tl"														<< endl;
    ct << ""																		<< endl;
    ct << "xlabel '$t$'"															<< endl;
    ct << "ylabel '$X$ [$\\%$]'"													<< endl;
    ct << ""																		<< endl;
    ct << "marker bullet"															<< endl;
    ct << "line-style no"															<< endl;
    ct << "plot @'$1:$6*100' /legend 'real'"										<< endl;
    ct << ""																		<< endl;
    ct << "marker no"																<< endl;
    ct << "line-style solid"														<< endl;

    for (FitterBase* fitter: simulation.getFitters())
    {
        if (fitter->isPlotEnabled())
        {
            ct << "" << endl;
            ct << "math  /xrange '" << fitter->getTimeMin() << ":" << fitter->getTimeMax() << "' /samples 200"	<< endl;
            ct << "plot  '100 * (1 - exp(-1 * " << fitter->getFitK() << " * x ** " << fitter->getFitN() << "))' /legend \"fit (\\$X_{fit}=1-e^{-" << fitter->getFitK() << " \\\\cdot t^{" << fitter->getFitN() << "}}\\$)\""	<< endl;
        }
    }

    ct.close();

}

string RenderCtiogaProgress::buildCt2Filename(const SimulationBase &simulation, string ct2Name )
{
    string filename = basename + "-" + ct2Name + ".ct2";


    if (simulation.getOutputDir() != "")
        filename = simulation.getOutputDir() + "/" + filename;

    return filename;
}


void RenderCtiogaProgress::createCtLog(SimulationBase& simulation)
{
    ofstream ct;
    ct.open(buildCt2Filename(simulation, "log"));

    ct << "title 'Cluster grow'" 							<< endl;
    ct << "legend-inside tl" 								<< endl;
    ct << ""						 						<< endl;
    ct << "xlabel '$\\ln(t)$'" 								<< endl;
    ct << "ylabel '$\\ln(-\\ln(1-X))$'"						<< endl;
    ct << ""						 						<< endl;
    ct << "marker bullet" 									<< endl;
    ct << "line-style no" 									<< endl;
    ct << "plot @'$2:$7' /where 'y>=-1e10 and y<+1e10' /legend 'real'"	<< endl;
    ct << ""						 						<< endl;
    ct << "marker no" 										<< endl;
    ct << "line-style solid" 								<< endl;

    for (FitterBase* fitter: simulation.getFitters())
    {
        if (fitter->isPlotEnabled())
        {
            ct << "" << endl;
            ct << "math  /xrange '" << log(fitter->getTimeMin()) << ":" << log(fitter->getTimeMax()) << "' /samples 200"	<< endl;
            ct << "plot  '" << fitter->getFitN() << " * x + log(" << fitter->getFitK() << ")' /legend \"fit (\\$n=" << fitter->getFitN() << ",k=" << fitter->getFitK() << ",RMSE=" << fitter->getFitRMSE() << "\\$)\""	<< endl;
        }
    }

    ct.close();
}




