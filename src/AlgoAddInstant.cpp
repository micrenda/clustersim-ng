#include "AlgoAddInstant.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

AlgoAddInstant::AlgoAddInstant(unsigned int quantity): quantity(quantity), added(false)
{	
	
}

int AlgoAddInstant::add(SimulationBase& simulation)
{
	return quantity;
}


float AlgoAddInstant::getNextExecution(SimulationBase& simulation)
{
	if (!added)
	{
		added = true;
		return 0;
	}
	else
		return -1;
}
