#include <algorithm>
#include <stdexcept>
#include <cmath>
#include <limits>
#include <SimulationVectorial.hpp>
#include <sys/stat.h>
#include <iostream>

#include "SimulationBase.hpp"
#include "AlgoAddBase.hpp"
#include "AlgoGrowBase.hpp"
#include "AlgoPosBase.hpp"
#include "FitterBase.hpp"
#include "RenderBase.hpp"
#include "World.hpp"
#include "Cluster.hpp"
#include "Schedulable.hpp"
 
using namespace clustersim;
using namespace std;

SimulationBase::SimulationBase(World& world): world(world), defaultClock(ClockTimer())
{

}

SimulationBase::~SimulationBase()
{
}


float SimulationBase::getTransformedRatio()
{
	return getTransformedVolume() / world.volume;
}

void SimulationBase::start()
{
	//if (world  == nullptr)
	//	throw logic_error( "The 'world' property was not set" );
		
	if (algoAdd  == nullptr)
		throw logic_error( "The 'algoAdd' property was not set" );
	if (algoPos  == nullptr)
		throw logic_error( "The 'algoPos' property was not set" );
	if (algoGrow == nullptr)
		throw logic_error( "The 'algoGrow' property was not set" );
	
	currentTime = 0;
		
	algoAdd->initialize(*this);
	algoPos->initialize(*this);
	algoGrow->initialize(*this);
	
	for (FitterBase* fitter: fitters)
		fitter->initialize(*this);
	
	for (RenderBase* render: renders)
		render->initialize(*this);
	
	scheduleNextExecution(algoAdd);
	scheduleNextExecution(algoGrow);
	
	for (FitterBase* fitter: fitters)
		scheduleNextExecution(fitter);
	
	for (RenderBase* render: renders)
		scheduleNextExecution(render);
}
/**
 * This method will progress the simulation. This means it will execute all the schedules and will increase the currentTime as needed.
 *
 *
 *
 * @param advanceTime If is false it will not allow the time to increase and will just complete the schedules list. If true, it will allow to increase the current time.
 * @return true if there are more elements scheduled to execute, false otherwise.
 */
bool SimulationBase::progress(bool advanceTime)
{
	set<Schedule>::iterator scheduleIt = schedules.begin();
	
	if (scheduleIt != schedules.end())
	{
		Schedule schedule = *scheduleIt;

		if (advanceTime || currentTime == schedule.when)
			currentTime = schedule.when;
		else
			return	false;
		
		// Removing the current element
		schedules.erase(scheduleIt);
		
		// Perform specific actions
		Schedulable* action = schedule.what;
		
		if (dynamic_cast<AlgoAddBase*>(action))
		{
			this->executeAdd();
		}
		else if (dynamic_cast<AlgoGrowBase*>(action))
		{
			this->executeGrow();
		}
		else if (RenderBase* render = dynamic_cast<RenderBase*>(action))
		{
			this->executeRender(*render);
		}
		else if (FitterBase* fitter = dynamic_cast<FitterBase*>(action))
		{
			this->executeFitter(*fitter);
		}
		else
			throw logic_error( "Internal error: Unable to detect the scheduled instance. Please contact the developers." );
		
		scheduleNextExecution(action);

		return true;
	}
	
	
	return false;
}


void SimulationBase::scheduleNextExecution(Schedulable* action)
{
		float whenNext = action->getNextExecution(*this);
		
		if (whenNext >= currentTime)
		{
			Schedule newSchedule;
			newSchedule.what = action;
			newSchedule.when = whenNext;
			schedules.insert(newSchedule);
		}
	
}

void SimulationBase::executeAdd()
{
	int qty = algoAdd->add(*this);
	
	for (int i = 0; i < qty; i++)
	{
		// This must be executed atomic in multi-threaded environmentnt
		Cluster* newCluster = this->clusterCreate(clusters.size(), currentTime);

        this->clusterPlacePre(*newCluster);
		
		if (!configAllowInTransformed)
		{
			bool inside = getClusterAtPoint((*newCluster).center) != nullptr;

			if (!inside)
            {
                clusters.push_back(newCluster);
                this->clusterPlacePost(*newCluster);
            }
		}
		else
        {
            clusters.push_back(newCluster);
            this->clusterPlacePost(*newCluster);
        }
	}
}

void SimulationBase::executeGrow()
{
    clusterGrowAllPre();
	for (Cluster* cluster: clusters)
		if (cluster->growing) clusterGrow(*cluster);
    clusterGrowAllPost();
}

void SimulationBase::executeRender(RenderBase& render)
{
    float transformedRatio = getTransformedRatio();
    if (!render.isHasVolumeRange() || (transformedRatio >= render.getVolumeRangeFrom() && transformedRatio <= render.getVolumeRangeTo()))
    {
        render.execute(*this);
    }
}

void SimulationBase::executeFitter(FitterBase& fitter)
{

    float transformedRatio = getTransformedRatio();
    if (!fitter.isHasVolumeRange() || (transformedRatio >= fitter.getVolumeRangeFrom() && transformedRatio <= fitter.getVolumeRangeTo()))
    {
	    fitter.execute(*this);
    }
}

void SimulationBase::stop()
{
	for (FitterBase* fitter: fitters)
		fitter->finalize(*this);
	
	for (RenderBase* render: renders)
		render->finalize(*this);
}

Cluster* SimulationBase::clusterCreate(unsigned int id, float creationTime)
{
	return new Cluster(world.dimensions, id, creationTime);
}

void SimulationBase::clusterPlacePre(Cluster &cluster)
{
	algoPos->place(*this, cluster);
}

void SimulationBase::clusterPlacePost(Cluster &cluster)
{

}

void SimulationBase::clusterGrow(Cluster& cluster)
{
	if (currentTime > cluster.lastGrowTime)
	{
		float grow = algoGrow->grow(*this, cluster);
		cluster.radius += grow;
		cluster.lastGrowTime = currentTime; 
	}
}

void SimulationBase::clusterGrowAllPre()
{

}

void SimulationBase::clusterGrowAllPost()
{

}

unsigned int SimulationBase::getClusterCount()
{
	return clusters.size();
}

Cluster& SimulationBase::getCluster(unsigned int c)
{
	return *clusters[c];
}


float SimulationBase::getTotalVolume()
{
	return world.volume;
}

float SimulationBase::getLogCurrentTime()
{
	return log(currentTime);
}

float SimulationBase::getLogTransformedRatio()
{
	return log(-log(1.f - getTransformedRatio()));
}



float SimulationBase::distance(vector<float>& point1, vector<float>& point2)
{
	float buffer = 0;
	for (unsigned char d = 0; d < world.dimensions; d++)
	{
		float dist = abs(point2[d] - point1[d]);

        dist = min(dist, world.sizes[d] - dist); // For periodic boundaries
		
		if (d == 0)
			buffer = dist;
		else
			buffer = hypotf(buffer, dist);
	}
	
	return buffer;
}





void SimulationBase::makeOutputDir()
{
	if (!outputDir.empty())
	{
		mkdir(outputDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	}

}

void SimulationBase::execute(float toVolume)
{
	double ratio;

	do
	{
		ratio = getTransformedRatio();
	}
    while (progress(ratio <= toVolume && ratio < 1.0));
}


