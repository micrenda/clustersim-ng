#include "AlgoAddSporadic.hpp"
#include "ClockTimer.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

AlgoAddSporadic::AlgoAddSporadic(unsigned int quantity): quantity(quantity)
{	
	
}

void AlgoAddSporadic::initialize(SimulationBase& simulation)
{
	AlgoAddBase::initialize(simulation);
	generator.seed(std::random_device()());
	
	distribution = std::uniform_real_distribution<float>(0, simulation.defaultClock.getPeriod());
}

int AlgoAddSporadic::add(SimulationBase& simulation)
{
	return chain.empty() ? 0 : 1; 
}


float AlgoAddSporadic::getNextExecution(SimulationBase& simulation)
{
	if (chain.empty())
	{
		if (refill)
		{
			for (unsigned int i = 0; i < quantity; i++)
				chain.insert(simulation.getCurrentTime() + distribution(generator));
				
			refill = false;
			nextRefill = simulation.getCurrentTime() + simulation.defaultClock.getPeriod();
		}
		else
		{
			refill = true;
			return nextRefill;
		}
	}

	float t = *chain.begin();
	chain.erase(chain.begin());
	return t; 
}
