#include "AlgoPosRandom.hpp"

namespace clustersim {
class SimulationBase;
}  // namespace clustersim

using namespace clustersim;

AlgoPosRandom::AlgoPosRandom()
{
}
		 
void AlgoPosRandom::initialize(SimulationBase& simulation)
{
	AlgoPosBase::initialize(simulation);
	generator.seed(std::random_device()());
}

