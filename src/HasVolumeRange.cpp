#include "HasVolumeRange.hpp"

using namespace clustersim;

HasVolumeRange::HasVolumeRange(float from, float to) :
		hasVolumeRange(true), volumeRangeFrom(from), volumeRangeTo(to)
{}

HasVolumeRange::HasVolumeRange() :
		hasVolumeRange(false), volumeRangeFrom(0.), volumeRangeTo(0.)
{

}

void HasVolumeRange::setVolumeRange(const float from, const float to)
{
		hasVolumeRange = true;
		
		volumeRangeFrom = from;
		volumeRangeTo   = to;
}

void HasVolumeRange::clearVolumeRange()
{
		hasVolumeRange = false;
		
		volumeRangeFrom = 0.;
		volumeRangeTo   = 0.;
}
