# - Try to find PStreams
# Once done this will define
#  PSTREAMS_FOUND - System has PStreams
#  PSTREAMS_INCLUDE_DIRS - The PStreams include directories
#  PSTREAMS_LIBRARIES - The libraries needed to use PStreams
#  						(Empty because PStreams has only a .h file)

find_path(PSTREAMS_INCLUDE_DIR pstreams/pstream.h HINTS ""   PATH_SUFFIXES pstreams )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(PStreams  DEFAULT_MSG PSTREAMS_INCLUDE_DIR)

mark_as_advanced(PSTREAMS_INCLUDE_DIRS PSTREAMS_LIBRARIES )

set(PSTREAMS_LIBRARIES "" )
set(PSTREAMS_INCLUDE_DIRS ${PSTREAMS_INCLUDE_DIR} )
