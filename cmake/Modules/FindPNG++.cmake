# Clustersim
# ---------
#
# Try to find PNG++
#
# Once done this will define
#
# ::
#
#   PNG++_FOUND 		- system has PNG++
#   PNG++_INCLUDE_DIR 	- the PNG++ include directory
#   PNG++_LIBRARIES 	- Link these to use BZip2
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.



find_path(PNG++_INCLUDE_DIR png++/png.hpp PATH_SUFFIXES include)

if (NOT PNG++_LIBRARIES)
    find_library(PNG++_LIBRARIES NAMES png libpng PATH_SUFFIXES lib)
endif ()

# handle the QUIETLY and REQUIRED arguments and set CLUSTERSIM_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PNG++ REQUIRED_VARS PNG++_LIBRARIES PNG++_INCLUDE_DIR)

mark_as_advanced(PNG++_INCLUDE_DIR)
