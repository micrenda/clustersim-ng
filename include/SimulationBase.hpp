#pragma once
#include <set>
#include <vector>
#include <string>

#include "ClockTimer.hpp"
#include "Schedule.hpp"

namespace clustersim
{

    class Cluster;
    class RenderBase;
    class Schedulable;
    class World;

	class AlgoAddBase;
	class AlgoGrowBase;
	class AlgoPosBase;
	class FitterBase;


	class SimulationBase
	{

	public:

		SimulationBase(World& world);
		virtual ~SimulationBase();



		virtual float getCurrentTime() 		{ return currentTime; }
		virtual float getTransformedRatio();

		virtual float getLogCurrentTime();
		virtual float getLogTransformedRatio();

		virtual float getTransformedVolume() = 0; // Pure virtual
		virtual float getTotalVolume();

		virtual float getClusterVolume(Cluster& cluster) = 0; // Pure virtual

		virtual float  distance(std::vector<float>& point1, std::vector<float>& point2);

		virtual Cluster*	getClusterAtPoint(std::vector<float>& point) = 0;


		virtual void  start();
        virtual void  execute(float toVolume = 1.);
		virtual void  stop();


		virtual unsigned int getPreferredPixelSize(unsigned short axis) = 0; // Pure virtual

        const std::string getOutputDir() const { return outputDir; };
        void setOutputDir(const std::string& outputDir){ SimulationBase::outputDir = outputDir; };

    protected:

		World&	world;


		float	currentTime = 0;
		bool	running     = false;

		AlgoAddBase*	algoAdd  = nullptr;
		AlgoPosBase*	algoPos  = nullptr;
		AlgoGrowBase*	algoGrow = nullptr;

		std::vector<FitterBase*>	fitters;


		std::vector<Cluster*> 	 clusters;
		std::vector<RenderBase*>  renders;

		std::multiset<Schedule> schedules;

		std::string     outputDir = "";

	public:

		ClockTimer defaultClock;


		virtual Cluster& getCluster(unsigned int c);
		virtual unsigned int getClusterCount();


		virtual AlgoAddBase*	getAlgoAdd()  { return algoAdd;  }
		virtual AlgoPosBase*	getAlgoPos()  { return algoPos;  }
		virtual AlgoGrowBase*	getAlgoGrow() { return algoGrow; }

		virtual void setAlgoAdd(AlgoAddBase*   algoAdd)  { this->algoAdd  = algoAdd;  }
		virtual void setAlgoPos(AlgoPosBase*   algoPos)  { this->algoPos  = algoPos;  }
		virtual void setAlgoGrow(AlgoGrowBase* algoGrow) { this->algoGrow = algoGrow; }

		virtual void addRender(RenderBase* render) { renders.push_back(render); }

		virtual void addFitter(FitterBase* fitter) { fitters.push_back(fitter); }

		virtual std::vector<FitterBase*> getFitters() { return fitters; }

		virtual World& getWorld() { return world;};



	protected:

        virtual bool progress(bool advanceTime);

		virtual void scheduleNextExecution(Schedulable* action);

		virtual void executeAdd();
		virtual void executeGrow();
		virtual void executeRender(RenderBase& render);
		virtual void executeFitter(FitterBase& fitter);

		virtual Cluster*	clusterCreate(unsigned int id, float creationTime) = 0; // Pure virtual

		virtual void	clusterPlacePre (Cluster &cluster);
		virtual void    clusterPlacePost(Cluster &cluster);

		virtual void 	clusterGrowAllPre();
		virtual void 	clusterGrow(Cluster &cluster);
		virtual void 	clusterGrowAllPost();


		/**
		 * \brief Allows to place a new cluster inside an existing cluster
		 *
		 * This property specify if a new cluster is allowed in the volume transformed by
		 * another cluster.
		 *
		 * The Avrami model does allows the creation and grow of new clusters inside the
		 * transformed volume, but the pysical reality does not.
		 */
		bool    configAllowInTransformed = false;


	public:

        virtual bool isConfigAllowInTransformed() 		 { return configAllowInTransformed; }

		/**
		 * \brief Set the value of #configAllowInTransformed
		 */
		virtual void setConfigAllowInTransformed(bool value) {this->configAllowInTransformed = value; }


        void makeOutputDir();
    };

}
