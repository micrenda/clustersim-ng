#pragma once
#include <fstream>
#include <string>

#include "RenderBase.hpp"

namespace clustersim {

    class SimulationBase;

	class RenderCsvBase: public RenderBase
	{
		public:
		RenderCsvBase(bool periodic, string basename);
		
		virtual void initialize(SimulationBase& simulation) override;
		virtual void finalize(SimulationBase& simulation) override;
		
		
		
		protected:
		string basename;
		ofstream csvfile;
		
		virtual string buildFilename(const SimulationBase &simulation);
	};
}
