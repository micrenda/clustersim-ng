#pragma once

namespace clustersim
{

    class SimulationBase;

	class Schedulable
	{
		public:
        virtual float getNextExecution(SimulationBase& simulation) = 0; // pure virtual
	};
}
