#pragma once
#include <vector>

#include "ClusterColorGradientBase.hpp"

namespace clustersim {
	class Cluster;
	class SimulationBase;
		  
	class ClusterColorGradientAge: public ClusterColorGradientBase
	{
		public:
		ClusterColorGradientAge(std::vector<unsigned int> colors, float maxAge);
		virtual ~ClusterColorGradientAge(); // Pure virtual
		
		virtual void initialize(SimulationBase& simulation) 						override;
		virtual unsigned int getColor(SimulationBase& simulation, Cluster& cluster) override;
		virtual void finalize(SimulationBase& simulation) 							override; 

		protected:
		float maxAge;
	};
}
