#pragma once
#include <vector>

#include "ClusterColorGradientBase.hpp"

namespace clustersim
{
	class Cluster;
	class SimulationBase;
		  
	class ClusterColorGradientBirth: public ClusterColorGradientBase
	{
		public:
		ClusterColorGradientBirth(std::vector<unsigned int> colors, float maxTime);
		virtual ~ClusterColorGradientBirth(); // Pure virtual
		
		virtual void initialize(SimulationBase& simulation) 						override;
		virtual unsigned int getColor(SimulationBase& simulation, Cluster& cluster) override;
		virtual void finalize(SimulationBase& simulation) 							override; 
		
		


		protected:
		float maxTime;
	};
}
