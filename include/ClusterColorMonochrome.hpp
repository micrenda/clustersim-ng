#pragma once

#include "ClusterColorBase.hpp"

namespace clustersim
{
    class ClusterColorMonochrome : public ClusterColorBase
    {
    public:

        ClusterColorMonochrome();

        ClusterColorMonochrome(unsigned int color);

        virtual ~ClusterColorMonochrome() override;

        virtual void initialize(SimulationBase &simulation) override;

        virtual void finalize(SimulationBase &simulation) override;

        unsigned int getColor() const { return color; }

        void setColor(unsigned int color) { ClusterColorMonochrome::color = color; }

        virtual unsigned int getColor(SimulationBase &simulation, Cluster &cluster) override;

    protected:
        unsigned int color = 0x008bba;
    };
}


