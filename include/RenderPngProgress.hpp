#pragma once
#include <string>

#include "RenderPngBase.hpp"

namespace clustersim
{
	class SimulationBase;

	class RenderPngProgress: public RenderPngBase
	{
		public:
		RenderPngProgress(string basename): RenderPngProgress(basename, clusterColor) {};
		RenderPngProgress(string basename, ClusterColorBase* clusterColor): RenderPngBase(true, basename, clusterColor) {};
		
		virtual void initialize (SimulationBase& simulation)	override;
		virtual void execute    (SimulationBase& simulation)	override;
		virtual void finalize   (SimulationBase& simulation)	override;
		
		protected:
		
		virtual string buildFilename(const SimulationBase &simulation, unsigned int i);
		
		unsigned int currentFrame = 0;
	};
}
