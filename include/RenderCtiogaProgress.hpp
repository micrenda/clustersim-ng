#pragma once

#include "RenderCsvProgress.hpp"

namespace clustersim
{

    class RenderCtiogaProgress : public RenderCsvProgress
    {
    protected:
    public:
        RenderCtiogaProgress(string basename);

    protected:
        virtual void createCtLin(SimulationBase& simulation);
        virtual void createCtLog(SimulationBase& simulation);

        string buildCt2Filename(const SimulationBase &simulation, string ct2Name);

    public:
        virtual void finalize(SimulationBase &simulation) override;


    };

}