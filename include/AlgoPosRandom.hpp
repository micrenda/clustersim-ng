#pragma once
#include <random>

#include "AlgoPosBase.hpp"

namespace clustersim
{
	class SimulationBase;

	class AlgoPosRandom:  public AlgoPosBase
	{
	
	public:
	
		/**
		 * Default constructor.
		 * It will use #default_random_engine to generate random numbers
		 */
		 AlgoPosRandom();
		 
		 /**
		  * Initialize the distribution model for every axis
		  * 
		  */
		 virtual void initialize(SimulationBase& simulation);
		 
		 
	protected:
		std::default_random_engine generator;
		
	};

}
