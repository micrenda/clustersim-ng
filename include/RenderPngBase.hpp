#pragma once
#include <fstream>
#include <map>
#include <string>

#include "RenderBase.hpp"
#include "SimulationBase.hpp"
#include "ClusterColorMonochrome.hpp"

namespace clustersim
{

	class ClusterColorBase;



	
	class RenderPngBase: public RenderBase
	{
		public:
		RenderPngBase(bool periodic, string basename);
		RenderPngBase(bool periodic, string basename, ClusterColorBase* clusterColor);
		
		virtual void initialize(SimulationBase& simulation) override;
		virtual void finalize(SimulationBase& simulation) override;
		
		
		virtual void setPreferedWidth(unsigned int value)  { preferedWidth  = value; };
		virtual void setPreferedHeight(unsigned int value) { preferedHeight = value; };
		virtual unsigned int getPreferedWidth()  { return preferedWidth;  };
		virtual unsigned int getPreferedHeight() { return preferedHeight; };
		
		virtual void setAxisI(unsigned char value) { axisI = value; };
		virtual void setAxisJ(unsigned char value) { axisJ = value; };
		virtual unsigned char getAxisI() { return axisI; };
		virtual unsigned char getAxisJ() { return axisJ; };
		
		virtual bool  hasCutLevel(unsigned char axis);
		virtual float getCutLevel(unsigned char axis);
		virtual void  setCutLevel(unsigned char axis, float level);

        unsigned int getBackgroundColor() const { return backgroundColor; }
        void setBackgroundColor(unsigned int color) { backgroundColor = color;  }



		
		
		protected:
		string basename;
		
		unsigned int width, height;
		
		unsigned int preferedWidth  = 0;
		unsigned int preferedHeight = 0;
		
		unsigned char axisI		= 0;
		unsigned char axisJ		= 1;

		unsigned int backgroundColor = 0xffffff;
		
		map<unsigned char,float> cuts;
		

		ClusterColorBase*		clusterColor 	= nullptr;
		
		protected:
		virtual void checkConfig(SimulationBase& simulation);
		
		ClusterColorMonochrome defaultClusterColor;
	};
}
