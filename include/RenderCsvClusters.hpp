#pragma once
#include <string>

#include "RenderCsvBase.hpp"

namespace clustersim {
class SimulationBase;
}  // namespace clustersim



namespace clustersim
{
	class RenderCsvClusters: public RenderCsvBase
	{
		public:
		RenderCsvClusters(string basename): RenderCsvBase(false, basename) {};
		virtual void finalize(SimulationBase& simulation) override;
		
	};
}
