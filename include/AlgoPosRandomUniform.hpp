#pragma once
#include <random>
#include <vector>

#include "AlgoPosRandom.hpp"

namespace clustersim
{
	class Cluster;
	class SimulationBase;
	
	class AlgoPosRandomUniform: public AlgoPosRandom
	{
		
		
	public:
	
	
		void initialize(SimulationBase& simulation);
		
		void place(SimulationBase& simulation, Cluster& cluster) override;
		
	protected:
		/**
		 * Contains a #uniform_real_distribution object for every axis
		 */
		std::vector<std::uniform_real_distribution<float>> distributions;
		
	};

}
