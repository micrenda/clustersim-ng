#pragma once
#include <vector>

#include "ClusterColorBase.hpp"



namespace clustersim
{
		  
	class ClusterColorGradientBase: public ClusterColorBase
	{
		public:
		ClusterColorGradientBase(std::vector<unsigned int>& colors);
		virtual ~ClusterColorGradientBase() = 0; // Pure virtual
		

		protected:
		
		virtual unsigned int getColorGradRgb(float value);
		
		float valueStart = 0;
		float valueEnd   = 0;
		std::vector<unsigned int> colors;

	};
}
