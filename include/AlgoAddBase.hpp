#pragma once
#include <set>
#include "AlgoBase.hpp"
#include "Schedulable.hpp"

namespace clustersim
{
    class SimulationBase;

	/**
	 * Base class to exind your own add cluster algorithm.
	 * 
	 */
	class AlgoAddBase:  public AlgoBase, public Schedulable
	{
		
	public:
		
		
		/**
		 * Return the number of clusters to add at a given time.
		 * It uses #getNextExecution to know when it is called.
		 */
		virtual int add(SimulationBase& simulation) = 0;
		
		/**
		 * Says when to add the new clusters starting from the current time (relative time).
		 * If a negative value is returned, then this function and getQty will not be newer be
		 * called again
		 * 
		 */
		virtual float getNextExecution(SimulationBase& simulation) override = 0 ;
		
	};

}
