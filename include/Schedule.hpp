#pragma once
#include "Schedulable.hpp"

namespace clustersim
{
	class Schedule
	{
		public:
		Schedulable* what;
		float when;
		
		friend bool operator== (const Schedule &c1, const Schedule &c2) { return (c1.what == c2.what && c1.when == c2.when);};
		friend bool operator!= (const Schedule &c1, const Schedule &c2) { return !(c1 == c2);};
		
		friend bool operator>  (const Schedule &c1, const Schedule &c2)	{ return c1.when >  c2.when; };
		friend bool operator<= (const Schedule &c1, const Schedule &c2) { return c1.when <= c2.when; };
	 
		friend bool operator<  (const Schedule &c1, const Schedule &c2) { return c1.when <  c2.when; };
		friend bool operator>= (const Schedule &c1, const Schedule &c2) { return c1.when >= c2.when; };
	};
}
