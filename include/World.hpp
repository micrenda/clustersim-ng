#pragma once

#include <vector>

using namespace std;

namespace clustersim
{

    class World
    {

    public:

        World(vector<float> sizes);

        unsigned char dimensions;
        vector<float> sizes;
        float volume;

    };

}
