#pragma once

#include "DiscretizedPixel.hpp"

namespace clustersim
{
	class Cluster;

	class DiscretizedPixel
	{
		
	public:
		
		DiscretizedPixel()  {};
		~DiscretizedPixel() {};
		
		
	public:
		Cluster* cluster = nullptr;
	};

}
