#pragma once

namespace clustersim {

	class SimulationBase;

	class AlgoBase
	{
		
	public:
		AlgoBase();
		virtual ~AlgoBase();
	
		virtual void initialize(SimulationBase& simulation);
		virtual void finalize(SimulationBase& simulation);
	};

}
