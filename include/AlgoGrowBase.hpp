#pragma once
#include "Cluster.hpp"
#include "AlgoBase.hpp"
#include "Schedulable.hpp"



namespace clustersim
{
    class SimulationBase;

    class AlgoGrowBase:  public AlgoBase, public Schedulable
	{
		
	public:
	
	
		virtual float grow(SimulationBase& simulation, Cluster& cluster) = 0;
		
		/**
		 * Says when to add the new clusters starting from the current time (relative time).
		 * If a negative value is returned, then this function and getQty will not be newer be
		 * called again
		 * 
		 */
		virtual float getNextExecution(SimulationBase& simulation) override = 0;
	};

}
