#pragma once
#include <vector>


namespace clustersim
{
	
	class Cluster
	{
	
	public:
		Cluster(unsigned char dimensions, unsigned int id, float creationTime);
		
		
	public:
		unsigned int  id;
		std::vector<float> center;
		
		float radius;
		bool  growing;
		
		float creationTime;
		float lastGrowTime;
		
	};

}
