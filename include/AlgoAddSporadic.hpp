#pragma once
#include <random>
#include <set>

#include "AlgoAddBase.hpp"

namespace clustersim {
	class SimulationBase;

	/**
	 * It will add a fix number of cluster at every given interval
	 */
	class AlgoAddSporadic:  public AlgoAddBase
	{
		
	public:
		
		AlgoAddSporadic(unsigned int quantity);
		
		virtual void initialize(SimulationBase& simulation);
		
		int add(SimulationBase& simulation) override;
		
		float getNextExecution(SimulationBase& simulation) override;
		
		
		
		protected:
			unsigned int quantity;
			
		protected:
			std::default_random_engine 				generator;
			std::uniform_real_distribution<float> 	distribution;
			
			/** This set contains the time when each cluster must be added.  */
			std::multiset<float>					chain; 
			
			bool  refill = true;
			float nextRefill;
	};

}
