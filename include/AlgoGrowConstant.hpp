#pragma once
#include "AlgoGrowBase.hpp"

namespace clustersim
{
	class Cluster;
	class SimulationBase;

	class AlgoGrowConstant:  public AlgoGrowBase
	{
		
	public:
	
		/**
		 * Grow a cluster with a constant speed
		 * 
		 * #speed the grow speed (m/s)
		 */
		AlgoGrowConstant(float speed);
		
		virtual float grow(SimulationBase& simulation, Cluster& cluster);
		virtual float getNextExecution(SimulationBase& simulation);
		
	protected:
		float speed;
	};

}
