#pragma once

#include <limits>
#include <map>

#include "Schedulable.hpp"
#include "HasVolumeRange.hpp"

using namespace std;


namespace clustersim
{
	class SimulationBase; // Using foward declaration to avoid cyclic dependency.
					      // Really C++ has not a better way to handle this?
					      
	class FitterBase: public Schedulable, public HasVolumeRange
	{
		public:
		FitterBase(bool periodic);

		FitterBase(float volumeRangeFrom, float volumeRangeTo, bool periodic);

		virtual ~FitterBase();
		
		virtual void initialize(SimulationBase& simulation);
		virtual void execute(SimulationBase& simulation);
		virtual void finalize(SimulationBase& simulation);
	
		virtual float getNextExecution(SimulationBase& simulation) override;
		
		virtual float getFitN()    { return fitN;    };
		virtual float getFitK()    { return fitK;    };
		virtual float getFitMSE()  { return fitMSE;  };
		virtual float getFitRMSE() { return fitRMSE; };
		virtual float getTimeMin() { return timeMin; };
		virtual float getTimeMax() { return timeMax; };
		
		virtual void  setPlotEnabled(bool value) 	{ plotEnabled = value; };
		virtual bool  isPlotEnabled() 				{ return plotEnabled; };
		
		virtual unsigned int getPointsCount();
		
		protected:

		bool periodic;
		
		map<float, float> logValues;
		
		float fitN   = 0;
		
		/**
		 * \brief K avrami coefficient
		 * 
		 * When plotting the points in a logarithmic chart, the point will set into a straight line with equation:
		 * \f$ y = m x + q \f$
		 * 
		 * Out coefficient is related to q with the relation:
		 * \f$K = e^{q}\f$
		 */
		float fitK 	 = 0;
		
		/**
		 * \brief Mean Squared Error
		 * 
		 * This value is the average of the quared errors from the fit line:
		 * 
		 * \f$MSQ = \frac{1}{n} \sum_{i=1}^{n}\left ( \hat{Y} - Y_{i}\right )^{2}\f$
		 * 
		 * See also: \link <https://en.wikipedia.org/wiki/Mean_squared_error> {Mean Squared Error}
		 */
		float fitMSE = 0;
		
		/**
		 * \brief Root Mean Squared Error
		 * 
		 * This value is the average of the quared errors from the fit line:
		 * 
		 * \f$MSQ = \frac{1}{n} \sum_{i=1}^{n}\left ( \hat{Y} - Y_{i}\right )^{2}\f$
		 * 
		 * See also: \link <https://en.wikipedia.org/wiki/Root-mean-square_deviation> {Root Mean Squared Error}
		 */		
		float fitRMSE = 0;
		
		float timeMin =  numeric_limits<float>::max();
		float timeMax = -numeric_limits<float>::max();
		
		/**
		 * \brief Enable or disable the plotting of the fit
		 */
		bool  plotEnabled = true;
		
	};
}
