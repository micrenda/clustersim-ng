#pragma once
#include <vector>

#include "ClusterColorBase.hpp"

namespace clustersim
{
	class Cluster;
	class SimulationBase;

	class ClusterColorRandomAcid: public ClusterColorBase
	{
		public:
		ClusterColorRandomAcid();
		virtual ~ClusterColorRandomAcid(); // Pure virtual
		
		virtual void initialize(SimulationBase& simulation) override; 
		virtual unsigned int getColor(SimulationBase& simulation, Cluster& cluster) override;
		virtual void finalize(SimulationBase& simulation) override;

		protected:
		std::vector<unsigned int> color_map_acid;
	};
}
