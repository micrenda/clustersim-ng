#pragma once
#include <set>

#include "AlgoAddBase.hpp"

namespace clustersim {

    class SimulationBase;

	/**
	 * It will add a fix number of clusters at the begin and then it will not add any cluster anymore
	 */
	class AlgoAddInstant:  public AlgoAddBase
	{
		
	public:
		
		AlgoAddInstant(unsigned int quantity);
		
		
		/**
		 * Add a give quantity at the beginning and then stop
		 */
		int add(SimulationBase& simulation) override;
		
		/**
		 * It will newer call again #getNextExecution
		 */
		float getNextExecution(SimulationBase& simulation) override;
		
		
		protected:
		
			unsigned int quantity;
			bool added;
			
		
	};

}
