#pragma once
#include <string>

#include "RenderCsvBase.hpp"

namespace clustersim {
class FitterBase;
class SimulationBase;
}  // namespace clustersim


namespace clustersim
{

	class RenderCsvFit: public RenderCsvBase
	{
		public:
		RenderCsvFit(string basename);
		
		virtual void initialize(SimulationBase& simulation)	override;
		virtual void execute(SimulationBase& simulation)	override;
		virtual void finalize(SimulationBase& simulation)	override;
		
		
		
		protected:
		FitterBase* fitter;
	};
}
