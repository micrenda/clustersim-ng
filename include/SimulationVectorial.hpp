#pragma once

#include <random>
#include <vector>
#include <unordered_map>

#include "SimulationBase.hpp"
#include "RenderPngBase.hpp"

namespace clustersim {
class Cluster;
class World;
}  // namespace clustersim

namespace clustersim
{
	enum class ClusterSelectionMethod { BIGGEST, SMALLEST, OLDEST, NEWEST, NEAREST, FARTHEST, SPEED};
	
	class SimulationVectorial: public SimulationBase
	{
		
	public:
		
		SimulationVectorial(World& w);
		virtual ~SimulationVectorial();
		/**
		 * To calculate the transformed volume, there is no analytic function
		 * but we needs to use a statistical function. We will pick randomly 
		 * a lot of points inside the volume and we will check if they belong 
		 * to a cluster or not
		 * 
		 */
		virtual float getTransformedVolume() override;

		virtual unsigned int getPreferredPixelSize(unsigned short axis) override;

		virtual float getClusterVolume(Cluster& cluster) override;
		
		/**
		 * One of the main difficulties of \link #SimulationContinuous ia how to
		 * estimate the transformed volume. It is not right to sum the volume
		 * of the clusters, because they may be overlapped. The solution used
		 * is to pick a huge number of random points inside of the volume and check
		 * if they are inside a cluster or not. The ratio between the points
		 * falling inside a cluster and the total points gives a good estimation
		 * of the transformed ratio.
		 * 
		 * This setter allow to set how many points must be picked every time 
		 * we need to know the volume ration. Lower values are faster but less 
		 * accurate. The default value is 250,000 points.
		 */
		virtual void setConfigEstimateVolumePointCount(unsigned int value) {this->configEstimateVolumePointCount = value; };
		
		virtual unsigned int getConfigEstimateVolumePointCount() { return configEstimateVolumePointCount; };

		virtual float		getClusterScore(std::vector<float>& point, Cluster& cluster, ClusterSelectionMethod selectionMethod);

		virtual Cluster *getClusterAtPoint(std::vector<float> &point) override;

	protected:
		virtual Cluster*	clusterCreate(unsigned int id, float creationTime)  override;
		virtual void 		clusterPlacePre(Cluster &cluster) 	override;
		virtual void 		clusterPlacePost(Cluster &cluster) 	override;
		virtual void 		clusterGrow(Cluster& cluster) 	override;
		virtual bool		isPointInCluster(Cluster& cluster, std::vector<float>& point);

		unsigned int configEstimateVolumePointCount	= 250000;


		float transformedVolume			= 0;
		unordered_map<unsigned int, float> transformedClusterVolumes;

		bool  transformedVolumeValid	= false;
		
		std::default_random_engine generator;

		ClusterSelectionMethod  selectionMethod = ClusterSelectionMethod::SPEED;

	};

}
