#pragma once

namespace clustersim
{
    class ClockTimer
    {
    public:
        ClockTimer(float period = 1.): period(period){};
        float getPeriod() { return period; };
    protected:
        float period;
    };

}