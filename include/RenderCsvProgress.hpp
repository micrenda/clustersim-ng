#pragma once
#include <string>

#include "RenderCsvBase.hpp"

namespace clustersim
{

	class SimulationBase;

	class RenderCsvProgress: public RenderCsvBase
	{
		public:
		RenderCsvProgress(string basename): RenderCsvBase(true, basename) {};
		
		virtual void initialize (SimulationBase& simulation)	override;
		virtual void execute    (SimulationBase& simulation)	override;
		virtual void finalize   (SimulationBase& simulation)	override;

	};
}
